# Copyright Al Cramer 2021 ac2.71828@gmail.com
from tkalt import *
from PIL import Image,ImageTk

# dev/test program for tkalt. 

# This function is called when user clicks the close-window button.
# Return True to allow the close, False to cancel.
def can_close_window():
    # start dev code
    return True
    # end dev code
    return askyesnocancel('Are you sure you want to quit?')

# styles for various contexts
col_fg = '#DEDEDE'
col_bg = '#303030'
col_canvas_dark = '#454545'
col_bg_light = '#CFCFCF'
styles = {
    'menu': {
        'bg':'blue',
        'bg.entry':'purple',
        'fg.entry':'yellow',
        'bg.button':'blue',
        'fg':'white',
        'border':False,
        'border.entry':True,
        },
    'dropdown': {
        'bg': '#f1f1f1',
        'fg': 'black',
        'border':False,
        },
    '*': {
        'fg':col_fg,
        'bg':col_bg,

        'fg.entry':'black',
        'bg.entry':'pink',

        'fg.textbox':'black',
        'bg.textbox':'white',

        'fg.checkbox':'lime',
        'bg.checkbox':'green',

        'bg.tabbar':'#656565',
        'bg.canvas':'white',
        'bg.button':'red',
        'border.button':True,

        'control.optionmenu':'cyan',
        'border.optionmenu':True,
        },
}

# cmds and handlers

def show_msg(msg):
    # post a msg to the canvas
    can = ui.canvas
    can.delete('all')
    can.create_text(100,50,fill='red',text=msg)

def show_img(fn):
    # post an image to the canvas
    can = ui.canvas
    can.delete('all')
    img_pil = Image.open(fn)
    img_pil.thumbnail((175,175),Image.ANTIALIAS)
    can.tkimg = ImageTk.PhotoImage(img_pil)
    can.create_image(100,50,anchor='nw',image=can.tkimg)

def on_canvas_mousemove(ev):
    # on mousemove, post coord's to canvas
    show_msg('Mouse is at: %d,%d' % (ev.x,ev.y))

# ui constructors

# For tab1 local-style test
style1 = {'border':False,'hmargin':40,'vmargin':20,
        'bg':'yellow','fg':'black'}
style2 = {'border':True,'hmargin':4,'vmargin':5,
        'bg':'purple','fg':'orange'}
curstyle = style1

def maketab1():
    def on_but1():
        show_msg('You pressed Always On')

    def on_but2():
        show_msg('You pressed Sometimes On')

    def on_recolor_radio():
        ui.canvas['bg'] = ui.rb_recolor_canvas.get()

    def on_recolor_cntrl():
        ui.canvas['bg'] = ui.colorcntrl.get() 

    def on_local_style():
        global curstyle
        curstyle = style2 if curstyle == style1 else style1
        ui.localstylebut.set_style(curstyle)
        msg = '%s' % curstyle
        msg = msg[1:-1].replace("'",'').replace(',','\n')
        show_msg(msg)


    return [
        Button('Always On',on_but1,always_enabled=True),
        Button('Sometimes On',on_but2),
        Hr(),
        Button('Recolor Canvas:', on_recolor_radio),
        [
            Radiobutton('rb_recolor_canvas','pink','pink',
                onchange=on_recolor_radio),
            Radiobutton('rb_recolor_canvas','blue','blue'),
            Radiobutton('rb_recolor_canvas','white','white')
        ],
        Label('Recolor from control:'),
        ColorControl('colorcntrl','Recolor',on_recolor_cntrl),
        Hr(),
        Button('Local Style Button',
            on_local_style,
            id='localstylebut',
            local_style = style1)
        ]

def maketab2():
    def on_apply():
        show_msg(ui.textbox.get())

    def on_tab2colors():
        ui.canvas['bg'] = ui.tab2colors.get()

    def on_post():
        fn = askopenfilename()
        if fn != '':
            show_img(fn)

    return [
        Textbox('textbox',30,10),
        Button('Apply',on_apply),
        OptionMenu('tab2colors',
            ('white','pink','blue','black'),
            label='Canvas bg:',
            onchange=on_tab2colors),
        Button("Post Image",on_post)
        ]

def make_menu():

    class ToggleEnable(Button):
        def __init__(self,ui_id,onmsg,offmsg):
            self.ui_id = ui_id
            self.onmsg = onmsg
            self.offmsg = offmsg
            self.state = 'on'
            super().__init__(onmsg,self.toggle,True)

        def toggle(self):
            comp = getattr(ui,self.ui_id)
            if self.state == 'on':
                self.state = 'off'
                self.set_text(self.offmsg)
                disable_widget(comp)
            else:
                self.state = 'on'
                self.set_text(self.onmsg)
                enable_widget(comp)

    def on_set_radio1():
        ui.rbs.set('rb1')
        show_msg('radio set programatically. value: %s' % ui.rbs.get())

    def on_rb_change():
        show_msg('radio button onchange. value now: %s' % ui.rbs.get())
 
    def on_cb_change():
        show_msg('checkbox onchange. value now: %s' % ui.checkbox.get())

    def on_post():
        show_msg('Menu entry is: %s' % ui.identry.get()) 

    return [
            [
                DropdownMenu('Enable/Disable','dropdown', 
                    ToggleEnable('menu','Disable Menu','Enable Menu'),
                    ToggleEnable('tabcntrl',
                        'Disable Controls','Enable Controls')
                    ),
                Button('Post',on_post),
                Entry('identry',6,label='entry:'),
                Radiobutton('rbs', 'radio1','rb1',onchange=on_rb_change),
                Radiobutton('rbs', 'radio2','rb2'),
                Button("set radio1",on_set_radio1),
                Checkbox('checkbox','checkbox',onchange=on_cb_change),
                ImageButton(Image.open("./sun.png"),
                    lambda: show_img('sun.png')),
                ImageButton(Image.open("./moon.png"),
                    lambda: show_img('moon.png')),
            ],

        ]

# initialize tkalt
tkalt_init('My App',styles,can_close_window,'./star.png')

# construct the UI
Layout(None,None,w=700, h=400,
    N = Frame('menu','menu',make_menu),
    C = Canvas('canvas',None,w=50,h=40,weight=1,
            on_mousemove=on_canvas_mousemove),
    E = ViewSet('tabcntrl',None,
        ["Tab 1","Tab 2"],
        Frame('tab1',None,maketab1),
        Frame('tab2',None,maketab2),
        ),
).make()

# kick off the main event loop
mainloop()

