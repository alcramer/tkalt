## tkalt: The easy way to build UI's with Python

tkalt is an alternative way to build complex UI's using Python 
and tkinter.  The package supports:

- top-down, declarative construction of UI's. (No need to tinker with 
Tkinter's geometry managers).

- declarative styling of elements, similar to HTML CSS.

- systematic enable/disable of widgets.

- Viewsets and Tabbed Views (sets of views, such that only 1 view is 
visible at any given time).

- Dropdown menus.

tkalt is built on tkinter. To use tkalt, it helps to have some familiarity
with tkinter (frames & widgets). But if you've never used tkinter, this
document should tell you everything you need to know.

### Requirements
tkalt is a Python3 package. It
requires PIL (Python Imaging Library). This is not part of the standard
Python distribution, so you should pip install Pillow as needed:

		python -m pip install --upgrade Pillow

### Program structure

An app using tkalt has this structure:

		from tkalt import *

		styles = {
			# create the styles here
			...
		}

		tkalt_init("My App",styles)

		Layout(...
			# Build the UI here
			...

		).make()
		
		# Start the main event loop
		mainloop()

"styles" is a collection of styles: each style gives widget attributes
(background/foreground colors, etc.). for some specified context. The line

		tkalt_init("My App",styles)

initializes the package:  we pass in the name of the app, and 
a ref to the styles collection. 

		Layout(...
			# Build the UI here
			...

		).make()
		

constructs a Layout object, which defines the UI, then calls its
"make" method: that causes the underlying tkinter objects to be
created. Finally, 

		# Start the main event loop
		mainloop()

kicks off the main event loop. The app will sit, waiting and responding
to user actions.

### Layout Objects

tkalt supports two kinds of basic ui elements: Canvases and Widgets.
Canvases are display areas that you can draw on, paste images and text on,
etc. Widgets are things like Buttons, Entry objects (one line text entry
boxes), Textboxes (multiline text entry boxes), Labels, etc. Widgets are
laid out in containers, either in one row (menus and control bars)
or in multiple rows (control panels). These containers are called "Frames".

A second kind of container, called a "Layout", makes it possible
to create complex UI's.  A Layout 
object breaks the window into 5 regions, labled "N", "S", "E", "W", and
"C" (for North/South/East/West/Center). The picture is:

		************************************
		*               N                  *
		************************************
		*         *            *           *
		*    W    *     C      *     E     *
		*         *            *           *
		************************************
		*               S                  *
		************************************

When you create a Layout object, you pass the constructor one or more named 
arguments -- one named 'N', another named 'S', etc. These arguments can be 
either Canvases, Frames, or a another Layout object. After constructing the 
Layout object, call its "make" method: that causes the package to create 
all the tkinter objects needed for the UI, and position them to give 
the desired layout.   

In addition to Layout, tkalt adds another kind of object not found
in  tkinter: Viewset. A Viewset is set of one or more views, such that
only one view is visible at any given time. The views can be Canvases,
Frames, or Layout objects. Which view is shown is controlled programmatically
by the Viewset's  "show" method. When you construct a Viewset, you can
specify an optional "tablabels" value (a list of strings); tkalt will then
add a row of tab buttons, so the user can shift between views.

Here's some code that creates a UI.

		Layout(None,None,w=500,h=400,
    		N = Frame(None,'menu',make_menu),
    		C = Canvas('mycanvas',None,weight=1)
    		E = ViewSet('tabcntrl',None,
        		["Tab 1","Tab 2","Tab 3"],
        		Frame('tab1',None,makepane1),
        		Frame('tab2',None,makepane2),
        		Frame('tab3',None,makepane3),
        		),
		).make()

It consists of 3 parts: an upper menu bar across the top (North),
a Canvas below the menu bar (Center), and a tabbed control panel to the 
right of the Canvas (East). 

In constructors for UI components, the first argument is either
None or an id. If you need to call any of the methods associated 
with a component, you should give the component an id (say,'tabcntrl').
You can then access the object with this code:

		ui.tabcntrl

"ui" is a namespace object, created in tkalt, that provides access
to any components which were assigned id's.  Ex:

		ui.tabcntrl.show('tab1')

will show the first pane in the tabbed view set.
 

<a name="_styles"></a>
### Styles

Tkinter widgets have attributes which determine their appearance.
In tkalt, you set these attributes with a styles collection.
Ex:

		styles = {
			'menu': {
				'bg': 'black',
				'fg': 'white'
			},
    		'*': {
        		'fg':'#DEDEDE',
        		'fg.entry':'black',
        		'bg.entry':'white',
        		'fg.textbox':'black',
        		'bg.textbox':'white',
        		'bg':'#303030',
        		'bg.tabbar':'#656565',
        		'bg.canvas':'white',
			}
		}

This defines 2 styles, one named 'menu' and the other named '\*'.
The rules are:

1. The style values defined in the '\*' dictionary apply to all
UI components. 

2. A UI component can be assigned a 'styleid' attribute (say, 'menu').
If that attribute matches the name of a style defined in
the styles collection, then the values for that style override any values
established by the '\*' entry.

3. You don't have to specify a '\*' entry in your style collection. In that case
the default values for style attributes will be supplied by tkinter.

The attribute name/value pairs in general have the form:

		'bg':'#000000'

Here "bg" means "background-color" and "#000000" is an html-style 
color code (you can also use common color names like "black", "white",
"red", etc.).


You can also add a qualifier ('.textbox') to an attribute name,
restricting it to the specified widget class.
This example:

		'fg': 'white',
		'bg': 'black',
		'fg.textbox': 'black',
		'bg.textbox': 'white',
		'bg.canvas':'white'

will set 'fg' (foreground-color) to white and 'bg' to black 
for most components,
giving a dark theme to the UI. But the qualified entries 

		'fg.textbox': 'black',
		'bg.textbox': 'white',
		'bg.canvas':'white'

will display textboxes as black letters on a white background. Canvases
will also be white.

When you declare a UI component, arg0 is the component id and arg1 is the
styleid.  In this example:

		Layout(None,None,w=500,h=400,
    		N = Frame(None,'menu',make_menu),
    		C = Canvas('mycanvas',None,weight=1)
    		E = ViewSet('tabcntrl',None,
        		["Tab 1","Tab 2","Tab 3"],
        		Frame('tab1',None,makepane1),
        		Frame('tab2',None,makepane2),
        		Frame('tab3',None,makepane3),
        		),
		).make()

all of the UI components except the menu (the North frame)
have styleid of None, so they'll be assigned the '\*' style;
the menu will get the 'menu' style. Styleids propogate down
the tree. Suppose we wanted the tab panes to have some new style,
'cntrls'. After adding a section named 'cntrls' to the styles
collection, we would change this line: 

    		E = ViewSet('tabcntrl',None,

to:

    		E = ViewSet('tabcntrl','cntrls',

It's not necessary to change the declarations of the 3 Frames
which are the panes: because these are child nodes of the ViewSet
node, they will inherit the styleid of their parent.

### Adding Widgets to Frames

Here again is code that creates a UI.

		Layout(None,None,w=500,h=400,
    		N = Frame(None,'menu',make_menu),
    		C = Canvas('mycanvas',None,weight=1)
    		E = ViewSet('tabcntrl',None,
        		["Tab 1","Tab 2","Tab 3"],
        		Frame('tab1',None,makepane1),
        		Frame('tab2',None,makepane2),
        		Frame('tab3',None,makepane3),
        		),
		).make()

This line:

        		Frame('tab1',None,makepane1),

creates the first pane of the tabbed view. Arg0 is the id of this UI component.
Arg1 is the styleid for the pane (in this case, None). Arg2
("makepane1") is the name of a function which returns the 
widgets for the pane.

That function could look like this:

		def makepane1():
			return [
				Button('Button1',on_button1),
				[
					Button('Button2A',on_button2A),
					Button('Button2B',on_button2B),
				],
				Button('Button3',on_button3),
			]

Here we return 4 buttons (arg0 in the constructor is the
label, and arg1 is the name of the function called when the
button is pressed). The list structure of the return determines
how the buttons are laid-out in the frame. The convention is:

1. if the function returns a single widget, the frame will contain
a single row (aligned to top) and that row will contain the widget
(aligned to left).

2. if the function returns a list, each item in the list gives the
content of a row. So the example gives a 3-row layout such that
Button1 is on the first row, Button2A and Button2B are on the second,
and Button3 is on the third.

Note that by these conventions,

	return [
		Button('Button1',on_button1),
		Button('Button2',on_button2),
		Button('Button2',on_button2),
	]

gives a column of 3 buttons, and

	return [[
		Button('Button1',on_button1),
		Button('Button2',on_button2),
		Button('Button2',on_button2),
	]]

gives a row of buttons. So if you're building a menu or control
bar, you would use the second.

### Enable/Disable Widgets
The functions

		enable_widgets(*args)
		disable_widgets(*args)

will enable/disable widgets. The varargs *args* specifies the widgets.
For a given item in *args*:
- if the item is a reference to a widget ("ui.myentry") the widget is
enabled/disabled.
- if the item is a reference to a Frame or Layout object,
all widgets contained in it are enabled/disabled.
- if the item is the name of a function, any buttton that calls that function
when clicked is enabled/disabled.

## tkalt API

### *ui*: the namespace object
tkalt exports a namespace object that provides references to 
objects that were assigned ids. This declaration  

		Entry('myentry',6)

declares a 1-line text entry box ('6' is its width, in characters).
Its id is "myentry", so this expression:

		ui.myentry

is a reference to the Entry object. This code:

		ui.myentry.set('abcd')

will set the text to 'abcd'.

Notes:
- In general, *ui.mywidget* is a reference to the tkalt object 
named *mywidget*. The one exception concerns Canvas objects. If *mycanvas*
is the name of a tkalt Canvas, *ui.mycanvas* is a reference to the 
associated tkinter Canvas. 
	

### Styles
How styles work is described in detail [above](#_styles). Here we note that
a "style" is just a dictionary of name/value pairs, where "name" is
the name of a tkinter attribute:

		{
			'fg':'#DEDEDE',
			'fg.entry':'black',
			'bg.entry':'white',
			'fg.textbox':'black',
			'bg.textbox':'white',
			'bg':'#303030',
			'bg.tabbar':'#656565',
			'bg.canvas':'white',

		}

The attribute name can be either qualified or unqualified. If
unqualified ("fg") it applies to all widgets which have an "fg"
attribute. If qualified ("fg.textbox") it applies only to widgets
whose class name matches the qualifier. So these entries:

			'fg.textbox':'black',
			'bg.textbox':'white',

make textboxes display as black text on a white background.

The standard attribute names are:
* "fg","bg": foreground and background colors. Values are either HTML hex codes or common color names.
* "vmargin","hmargin": vertical and horizontal margins (in pixels) for widgets. Use these to increase the size of buttons and frames.
* "border": a boolean value. True means: draw a border around widgets. Default is False (no border). 

Additional attribute names are:
* "bg.tabbar": color of tab bar in a tab control
* "control.optionmenu": color of up/down triangle icon in an OptionMenu 


### Dialogs
tkinter offers a variety of popup dialogs. tkalt supports this subset:

		showinfo(msg):
    		# Post info popup.

		showerror(msg):
    		# Post error message popup.
		
		askyesnocancel(msg):
    		# Post yes/no/cancel popup.
		# Return boolean on yes/no, None on cancel.
		
		askopenfilename(initialdir=None,filetypes=()):
    		# Post select file popup. Returns empty string on cancel. 
			# See tkinter documentation for details on "filetypes" filter.

		askopenfilenames(initialdir=None,filetypes=()):
    		# Post select file(s) popup. Returns empty tuple on cancel.  
			# See tkinter documentation for details on "filetypes" filter.

		askdirectory():
    		# Post select directory popup. Returns empty string on cancel.

tkalt just calls the corresponding tkinter functions, passing along
the supplied arguments. If you need other tkinter dialogs,
this expression:

		ui.tk

gives access to tkinter. So this code:

		ui.tk.simpledialog.askfloat("MyAppName", "Scale Value")

prompts the user for a float value.



### UI Components

#### Layout Object
A Layout object arranges other UI Components (Frame, Canvas, ViewSet,
or another Layout object) in a specified configuration.

		Layout(id,styleid,w=None,h=None,
			N=None,S=None,E=None,W=None,C=None)

		id: id for this ui component
		styleid: styleid for this ui component
		w,h: width and height
		N,S,E,W,C: Subnodes of the layout object. 

		Methods:

		make()
		Call this on the root (top-most) Layout object in the UI. 

Notes:
- If this Layout object is root (topmost element) of the layout, "w" and "h" 
(width and height) specify the width and height of the app's window.
If the Layout object is nested (not the root), don't specify an explicit
"w" and "h": these will be set by tkalt.   
- "N,S,E,W,C" are UI components to be place at the specified
positons (North,South,East,West,and Center, where North is the top of
the window, South is the bottom, West is the left side, etc.)
Legal components are: Frame, Canvas, ViewSet, or another Layout object.


#### Frame Object
A Frame object is a container for Widgets, placed in one or
more rows.

		Frame(id,styleid,content)

		id: id for this ui component
		styleid: styleid for this ui component
		content: widgets contained in this frame.

		Methods:
		none

Notes:
- *content* is either a single widget, or a list such that each element
defines a row.

#### Canvas Object
Canvas wraps the tkinter class "Canvas": you can draw on it,
paste text and images, track mouse movements, etc. 


		Canvas(id,styleid,w=None,h=None,weight=1,
			on_mousedown=None,
			on_mouseup=None,
			on_mousemove=None,
			on_doubleclick=None)

			id: id for this ui component
			styleid: styleid for this ui component
			w: width in pixels,
			h: height in pixels,
			weight: layout weight 
			on_mousedown, etc: mouse event handlers

	Methods:
		none (but see note below on calling methods of the
		associated tkinter Canvas).


Notes:
- By default, the canvas expands to fill all available space
after neighboring frames have been laid out. To constrain horizontal
expansion, specify a width; to constrain vertical expansion, specify
a height.

- *weight* controls space allocation for adjacent canvases when
user resizes the window. The canvas with greater weight gets more
space.

- A sample event handler:
		def on_mousemove(ev):
			print('Mouse coordinates: %d, %d' % (ev.x,ev.y))

- If you create a canvas named "mycanvas", this code

		ui.mycanvas

accesses the tkinter Canvas. Use this to call its methods. Ex:

		def on_mousemove(ev):
			ui.mycanvas.delete('all')
			ui.mycanvas.create_text(100,100, fill='red', 
        		text="Mouse is at: %d,%d" % (ev.x,ev.y))

is a handler that paints the mouse coordinates to the canvas
when the mouse is moved.

#### ViewSet Object
A set of 1 or more views (Frames or Canvases), only one of which 
is visible at any given time. To show a view programatically,
call the *show* method, passing in the id of the view to be shown.
To create a tabbed view, pass in a list of tab labels to the
constructor (one for view).

		ViewSet(id,styleid,tablabels,*views)
			id: id for this ui component
			styleid: styleid for this ui component
			tablabels: list of labels for tabs
			*views: 1 or more views (Frame or Canvas)

		Methods:

		show(viewid)
			show the view with id *viewid*

Notes:
- Arg2 (*tablabels*) is a list of labels for tabs. If you specify
"None", the ViewSet will not have tabs and is controlled programmatically
by calling *show*

- the background color for the tabbar will be whatever value 'bg' has
in the current style. To change that to some contrasting value, include
		'bg.tabbar': '#454545'
in the style.


- ViewSets support 2 callback functions: *can_show* and *on_show*. By default these are
both None. If the viewset is named "myviewset", set them with this code:
		ui.myviewset.can_show = function_name
		ui.myviewset.on_show = function_name

*can_show* (if not None) is called immediately before showing a view.
The return value can cancel the show operation. Ex:

		def can_show(cur_view, new_view):
			if some-condition:
			# allow the veiw to change
				return True
			else:
			# cancel the change
				return False
*cur_view* is a ref to the current view. *new_view* is a ref to the
view about to be shown.

*on_show* (if not None) is called immediately after showing a view. Ex:

		def on_show(cur_view, old_view):
			# validate the new view

*cur_view* is a ref to the current view. *old_view* is a ref to the
previous view.

### Widgets

#### Label
Non-clickable text.

		Label(text,always_enabled=False,id=None,style_dictionary=None)
			text: the label text
			always_enabled: label text always displays as enabled
			id: ui id for the label.
			style_dictionary: dictionary of style attributes

		Methods:

		set_text(text):
			reset the label text

		set_style(style_dictionary):
			change style of widget. "style_dictionary" is a Python
			dictionary giving attribute id/value pairs:
			{ 'bg': '#000000', 'fg': '#ffffff'}

Notes:
- use "set_text" to display dynamic information to the user
("15 of 23 photos processed").

- The *style_dictionary* arg in the constructor lets you customize the
button style when you declare it. You can use the *set_style* method to
dynamically change the button's apperance.



#### Button
Some text (with optional background and border) that, when clicked,
executes a command. The command is either a function name, or a
lambda expression.

		Button(text,cmd,always_enabled=False, id=None, style_dictionary=None)
			text: text for button label
			cmd: function to be executed when button is clicked
			always_enabled: button cannot be disabled
			id: ui id for the button.
			style_dictionary: dictionary of style attributes

		Methods:

		set_text(new_label):
			reset the label of the button

		set_style(style_dictionary):
			change style of widget. "style_dictionary" is a Python
			dictionary giving attribute id/value pairs:
			{ 'bg': '#000000', 'fg': '#ffffff'}

Notes:
- To disable a button, call
		disable_widget(cmd)
where *cmd* is the name of the function bound to the button. To
enable a button, call
		enable_widget(cmd)

- By default, buttons are just clickable text. To give the button
  a border, add
		"border":True
  to the style.

- The *style_dictionary* arg in the constructor lets you customize the
button style when you declare it. You can use the *set_style* method to
dynamically change the button's apperance.

#### ImageButton
An image that, when clicked, executes a command.

		ImageButton(img_pil, cmd, size=48, always_enabled=False, id=None)
			img_pil: loaded image file, opened with PIL
			cmd: function to be executed when button is clicked
			size: max dimension (width or height) of display image in pixels.
			always_enabled: button cannot be disabled
			id: ui id for the button.

		Methods:

		None


Notes:
- *img_pil* is a loaded image file (".png" or ".jpg") opened with PIL:
		import PIL
		...
		img_pil = PIL.Image.open("./res/myimage.png")

- The image will be rescaled so its max dimension (width or height) is *size*

#### Entry
A 1-line text entry box.

		Entry(id,width,label=None)
			id: ui id for the widget
			width: width in characters of the box
			label: optional label for this widget

		Methods:

		get():
			get the text

		set(text):
			set the text

		set_default(text):
			set the text, if the box is empty (no effect if it 
			already contains text).

#### Textbox
A multi-line text entry box.

		Textbox(id,width,height)
			id: ui id for the widget
			width: width in characters of the box
			height: number of lines displayed

		Methods:

		get():
			get the text

		set(text):
			set the text

#### Checkbox

		Checkbox(id,label,onchange=None):
			id: ui id for the widget
			label: text for the checkbox label
			onchange: callback function when state is changed.

		Methods:

		get():
			Get state (True means box is checked)

		set(vBool):
			Set state (vBool == True means box is checked)

Notes:
- The onchange callback function should have form:
		def on_change():
			# do something.
			

#### Radiobutton
Checkboxes are square, radiobuttons are circles. Checkboxes are
solitary, while radiobutttons belong to sets:
selecting one element deselects any others.

		Radiobutton(id,label,value,onchange=None)
			id: ui id for the radiobutton 
			label: label for the radiobutton 
			value: value associated with the button (a str value)
			onchange: callback function when state is changed.

		Methods:

		get():
			get the value associated with the selected button.

		set(v):
			select the button whose value matches *v*
			

Notes:
- The *id* for the radiobutton is actually the id for the set: to make
2 radiobuttons belong to the same set, assign them the same id.

- The onchange callback function should have form:
		def on_change():
			# do something.
			

#### OptionMenu
A dropdown list of options.

		OptionMenu(id,options,label=None,onchange=None)
			id: ui id for the widget 
			options: the options (list of str's)
			label: label for the radiobutton 
			onchange: callback function when state is changed.

		Methods:

		get():
			get the selected option

		set(v):
			select the option "v", where v is one of the Str's passed
			to the constructor in the "options" list.
			
Note:
- The onchange callback function should have form:
		def on_change():
			# do something.

#### DropdownMenu
A MenuItem which, when clicked, expands to a drop dowm menu.

		DropdownMenu(title,styleid,*content):
			title: text for the MenuItem
			styleid: style to apply to the dropdown menu
			*content: varargs argument giving the content of the
				expanded drop down menu.

Notes:
- *title* is the text for Button which, when clicked, expands to the
dropdown menu.
- *styleid* can be None, in which case the dropdown menu inherits its
style from its parent.
- *content* is a sequence: each element is a row in the dropdown menu.

Ex: this code could be the "File" dropdown menu for a text editor.

		DropdownMenu('File',None,
			MenuItem('Open',on_file_open),
			MenuItem('Save',on_file_save),
			[MenuItem('Save as:',on_file_saveas), Entry('fn_saveas',12)],
			MenuItem('Quit',on_quit)
		)

The dropdown menu will contain 4 rows. The first, second, and last row will 
each consist of a single MenuItem. The third row will consist of two
widgets: a "Save as:" MenuItem, followed by a text entry box. Enclosing
two or more widgets in a list means: layout these widgets in the same row. 
 
#### Hr:
A horizontal rule (line) that splits a pane into regions.

		Hr():

		Methods:
			None

Note:
- The color will the value for the "fg" (foreground color) style
rule. To override this, include a qualified style rule. Ex:
		'fg.hr': '#717171'

will give a medium-grey line.
 
#### ColorControl:
A simple color picker: an Entry box for an html color code,
plus a reset button and a little swatch showing the selected color.

		ColorControl(id, reset_but_text,onchange=None):
			id: ui id for this widget 
			reset_but_text: label for reset button 
			onchange: callback function when user resets color 

		Methods:

		get():
			get the color (an Str giving html color code: "#ff0000"

		set(v):
			set the color

		set_default(v):
			set the color to "v", if current value is ""

Note:
- The onchange callback function should have form:
		def on_change():
			# do something.

Copyright Al Cramer 2021
